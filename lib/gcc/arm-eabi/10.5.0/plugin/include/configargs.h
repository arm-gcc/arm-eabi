/* Generated automatically. */
static const char configuration_arguments[] = "../gcc/configure -v --target=arm-eabi --prefix=/home/dgruzd/toolchain/scripts/gcc-arm --build=x86_64-linux-gnu --host=x86_64-linux-gnu --disable-docs --disable-gdb --disable-shared --disable-nls --disable-threads --disable-tls --disable-bootstrap --disable-browser-plugin --disable-cloog-version-check --disable-isl-version-check --disable-libgomp --disable-libitm --disable-libmudflap --disable-libsanitizer --disable-libssp --disable-libstdc__-v3 --disable-multilib --disable-ppl-version-check --disable-sjlj-exceptions --disable-vtable-verify --disable-werror --enable-checking=release --enable-languages=c --enable-lto --enable-plugin --enable-gold --enable-graphite=yes --enable-initfini-array --enable-ld=default --with-newlib --with-gnu-as --with-gnu-ld --without-included-gettext --with-target-system-zlib";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "arm7tdmi" } };
